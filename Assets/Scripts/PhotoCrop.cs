﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhotoCrop : MonoBehaviour {

	public Texture2D spriteToLoad;
	public int displayDim;

	// Use this for initialization
	void Start () {
		UpdateDisplayBlock();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Space))
		{
			UpdateDisplayBlock();
		}
	}

	void UpdateDisplayBlock()
	{
		Texture2D texToLoad = new Texture2D(displayDim, displayDim);
		int randX = Random.Range(displayDim, spriteToLoad.width - displayDim);
		int randY = Random.Range(displayDim, spriteToLoad.height - displayDim);
		Color[] displayBlock = spriteToLoad.GetPixels(randX, randY, displayDim, displayDim);

		texToLoad.SetPixels(displayBlock);
		texToLoad.Apply();
		GetComponent<RawImage>().texture = texToLoad;
	}
}
